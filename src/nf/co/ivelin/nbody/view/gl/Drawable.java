package nf.co.ivelin.nbody.view.gl;

import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.model.Body;

public interface Drawable {

	void draw(GL10 gl);
	
	void loadGLTexture(TextureManager textureManager);
	
	Body getBody();

}