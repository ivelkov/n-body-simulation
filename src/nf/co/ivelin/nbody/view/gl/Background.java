package nf.co.ivelin.nbody.view.gl;

import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.R;
import nf.co.ivelin.nbody.model.Body;

public class Background implements Drawable {
	
	private final Square mSquare;
	
	public Background(float width, float height) {
		mSquare = new Square(R.drawable.background, new Body(1, 0, 0, 0, width, height));
	}

	public void draw(GL10 gl) {
		mSquare.draw(gl);
	}

	public void loadGLTexture(TextureManager textureManager) {
		mSquare.loadGLTexture(textureManager);
	}
	
	public Body getBody() {
	    return mSquare.getBody();
	}

}
