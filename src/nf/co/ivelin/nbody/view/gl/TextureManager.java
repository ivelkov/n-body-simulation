package nf.co.ivelin.nbody.view.gl;

import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.SparseIntArray;

public class TextureManager {

	private final SparseIntArray mLoadedTextures = new SparseIntArray();

	public void init(GL10 gl, Context context) {
		loadTexture(gl, context, R.drawable.texture, false);
		loadTexture(gl, context, R.drawable.background, true);
	}

	private void loadTexture(GL10 gl, Context context, int textureId,
			boolean repeat) {
		final Bitmap bitmap = BitmapFactory.decodeResource(
				context.getResources(), textureId);
		final int[] textures = new int[1];
		// generate one texture pointer
		gl.glGenTextures(1, textures, 0);
		// ...and bind it to our array
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
		// create nearest filtered texture
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);
		if (repeat) {
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S,
					GL10.GL_REPEAT);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T,
					GL10.GL_REPEAT);
		}
		// Use Android GLUtils to specify a two-dimensional texture image from
		// our bitmap
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		// Clean up
		bitmap.recycle();
		mLoadedTextures.put(textureId, textures[0]);
	}

	public int getTexture(int resourceId) {
		return mLoadedTextures.get(resourceId);
	}

}
