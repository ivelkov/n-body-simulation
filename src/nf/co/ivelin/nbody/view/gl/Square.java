package nf.co.ivelin.nbody.view.gl;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.model.Body;
import nf.co.ivelin.nbody.model.Vector;

public class Square implements Drawable {

    private final FloatBuffer mVertexBuffer;

    private int mTexture;

    private final float mVertices[] = { -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f,
            0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.5f };

    private final float mColors[] = { 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.5f, 0.0f, 1.0f, 1.0f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f };

    private final byte mIndices[] = { 0, 4, 5, 0, 5, 1, 1, 5, 6, 1, 6, 2, 2, 6, 7, 2, 7, 3, 3, 7, 4, 3, 4, 0, 4, 7, 6, 4, 6, 5, 3, 0, 1, 3, 1, 2 };

    private FloatBuffer mTextureBuffer; // buffer holding the texture
                                        // coordinates

    private float texture[] = { 0.0f, 1.0f, // top left (V2)
            1.0f, 1.0f, // top right (V4)
            0.0f, 0.0f, // bottom left (V1)
            1.0f, 0.0f, // bottom right (V3)

    };

    private final int mTextureResourceId;

    private final Body mBody;

    private ByteBuffer mIndexBuffer;

    private FloatBuffer mColorBuffer;

    public Square(int textureResourceId, Body body) {
        mTextureResourceId = textureResourceId;
        scale(mVertices, body.getWidth(), body.getHeight());
        mBody = body;

        mIndexBuffer = ByteBuffer.allocateDirect(mIndices.length);
        mIndexBuffer.put(mIndices);
        mIndexBuffer.position(0);

        mVertexBuffer = ByteBuffer.allocateDirect(mVertices.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mVertexBuffer.put(mVertices);
        mVertexBuffer.position(0);

        mTextureBuffer = ByteBuffer.allocateDirect(texture.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextureBuffer.put(texture);
        mTextureBuffer.position(0);

        mColorBuffer = ByteBuffer.allocateDirect(mColors.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mColorBuffer.put(mColors);
        mColorBuffer.position(0);
    }

    private static void scale(float[] vertices, float width, float height) {
        final float ratioWidth = width / 1;
        final float ratioHeight = height / 1;
        for (int i = 0; i < vertices.length; i += 3) {
            vertices[i] *= ratioWidth;
        }
        for (int i = 1; i < vertices.length; i += 3) {
            vertices[i] *= ratioHeight;
        }

    }

    public void draw(GL10 gl) {
        gl.glPushMatrix();
        final Vector pos = mBody.getPosition();
        gl.glTranslatef((float) pos.get(0), (float) pos.get(1), (float) pos.get(2));
        gl.glRotatef(mBody.getRotateAngle(), (float) mBody.getRotateVector().get(0), (float) mBody.getRotateVector().get(1), (float) mBody.getRotateVector()
                .get(2));

        gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexture);

        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        // gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);

        gl.glFrontFace(GL10.GL_CW);

        // Point to our vertex buffer
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, mVertexBuffer);
        gl.glColorPointer(4, GL10.GL_FLOAT, 0, mColorBuffer);

        // gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, mTextureBuffer);
        gl.glColorPointer(3, GL10.GL_FLOAT, 0, mColorBuffer);

        // Draw the vertices as triangle strip
        // gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, mVertices.length / 3);
        gl.glDrawElements(GL10.GL_TRIANGLES, 36, GL10.GL_UNSIGNED_BYTE, mIndexBuffer);

        // Disable the client state before leaving
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        // gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
        gl.glPopMatrix();
    }

    public void loadGLTexture(TextureManager textureManager) {
        mTexture = textureManager.getTexture(mTextureResourceId);
    }

    public Body getBody() {
        return mBody;
    }

}