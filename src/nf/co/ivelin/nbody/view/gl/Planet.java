package nf.co.ivelin.nbody.view.gl;

import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.R;
import nf.co.ivelin.nbody.model.Body;

public class Planet implements Drawable {

    /** Textured planet. */
    private final Square mSquare;

    private final float mRotateStep;

    private final Body mBody;

    /**
     * Planet.
     * 
     * @param mass
     *            from 1 to 100.
     */
    public Planet(double mass, double x, double y, double z) {
        mBody = new Body(mass, x, y, z, 1, 1);
        mSquare = new Square(R.drawable.texture, mBody);
        final float rotateStep = (float) Math.max(0.1, (Math.random() * 2));
        final float polarity = Math.random() > 0.5 ? 1 : -1;
        mRotateStep = rotateStep * polarity;
    }

    public void draw(GL10 gl) {
        final float angle = (mBody.getRotateAngle() + mRotateStep) % 360;
        mBody.rotate(angle, 1, 1, -1);
        mSquare.draw(gl);
    }

    /**
     * @return the body
     */
    public Body getBody() {
        return mBody;
    }

    public void loadGLTexture(TextureManager textureManager) {
        mSquare.loadGLTexture(textureManager);
    }

    @Override
    public String toString() {
        return String.format("Planet [mBody=%s]", mBody);
    }

}
