package nf.co.ivelin.nbody.view;

import java.util.Collection;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import nf.co.ivelin.nbody.controller.Control;
import nf.co.ivelin.nbody.model.Body;
import nf.co.ivelin.nbody.model.WorldModel;
import nf.co.ivelin.nbody.view.gl.Drawable;
import nf.co.ivelin.nbody.view.gl.Square;
import nf.co.ivelin.nbody.view.gl.TextureManager;
import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

public class NBodyRenderer implements Renderer {

    private final Context mContext;

    private final WorldModel mWorld;

    private final TextureManager mTextureManager = new TextureManager();

    private final Control mControl;

    private boolean mInitializedTextures = true;

    public NBodyRenderer(Context context, WorldModel world, Control control) {
        mContext = context;
        mWorld = world;
        mControl = control;
    }

    public void onDrawFrame(GL10 gl) {
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();

        if (!mInitializedTextures) {
            for (Drawable drawable : mWorld.getDynamicObjects()) {
                drawable.loadGLTexture(mTextureManager);
            }
        }

        GLU.gluLookAt(gl, 0.0f, 0.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        final Drawable background = mWorld.getBackground();
        if (background != null) {
            background.draw(gl);
        }

        final Collection<Drawable> staticObjects = mWorld.getStaticObjects();
        for (Drawable object : staticObjects) {
            object.draw(gl);
        }

        final Collection<Drawable> dynamicObject = mWorld.getDynamicObjects();
        for (Drawable object : dynamicObject) {
            object.draw(gl);
        }
        gl.glFinish();
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (height == 0) { // Prevent A Divide By Zero By
            height = 1; // Making Height Equal One
        }

        gl.glViewport(0, 0, width, height); // Reset The Current Viewport
        gl.glMatrixMode(GL10.GL_PROJECTION); // Select The Projection Matrix
        gl.glLoadIdentity(); // Reset The Projection Matrix

        final float ratio = (float) width / (float) height;
        final int left = -10;
        final int right = 10;
        final float bottom = -10 / ratio;
        final float top = 10 / ratio;
        final Body leftBody = new Body(1, left, 0, 0, 1, top - bottom);
        final Body rightBody = new Body(1, right, 0, 0, 1, bottom - top);
        final Body topBody = new Body(1, left, top, 0, 2 * (right - left), 1);
        final Body bottomBody = new Body(1, left, bottom, 0, 2 * (right - left), 1);
        mWorld.setLeft(leftBody);
        mWorld.setRight(rightBody);
        mWorld.setTop(topBody);
        mWorld.setBottom(bottomBody);
        mWorld.addStaticObject(new Square(0, leftBody));
        mWorld.addStaticObject(new Square(0, rightBody));
        mWorld.addStaticObject(new Square(0, topBody));
        mWorld.addStaticObject(new Square(0, bottomBody));
        gl.glOrthof(left, right, bottom, top, 0.01f, 100.0f);

        gl.glMatrixMode(GL10.GL_MODELVIEW); // Select The Modelview Matrix
        gl.glLoadIdentity();

        mControl.start();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        mTextureManager.init(gl, mContext);
        gl.glEnable(GL10.GL_TEXTURE_2D); // Enable Texture Mapping ( NEW )
        gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f); // Black Background
        gl.glClearDepthf(1.0f); // Depth Buffer Setup

        gl.glEnable(GL10.GL_DEPTH_TEST); // Enables Depth Testing
        gl.glDepthFunc(GL10.GL_LEQUAL); // The Type Of Depth Testing To Do
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

        gl.glEnable(GL10.GL_SCISSOR_TEST);
        gl.glEnable(GL10.GL_DITHER);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE);
    }

}
