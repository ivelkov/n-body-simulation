package nf.co.ivelin.nbody.view;

import nf.co.ivelin.nbody.controller.Control;
import nf.co.ivelin.nbody.model.WorldModel;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class NBodySurfaceView extends GLSurfaceView {

	private final WorldModel mWorld;

	private final Control mControl;

	public NBodySurfaceView(Context context) {
		super(context);
		mWorld = new WorldModel();
		mControl = new Control(mWorld, this);
		// setEGLContextClientVersion(2);
		setRenderer(new NBodyRenderer(context, mWorld, mControl));
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		return mControl.onTouch(e);
	}

}
