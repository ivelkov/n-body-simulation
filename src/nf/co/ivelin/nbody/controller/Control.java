package nf.co.ivelin.nbody.controller;

import java.util.List;

import nf.co.ivelin.nbody.model.Body;
import nf.co.ivelin.nbody.model.Physics;
import nf.co.ivelin.nbody.model.Vector;
import nf.co.ivelin.nbody.model.WorldModel;
import nf.co.ivelin.nbody.view.NBodySurfaceView;
import nf.co.ivelin.nbody.view.gl.Drawable;
import nf.co.ivelin.nbody.view.gl.Planet;
import android.util.Log;
import android.view.MotionEvent;

public class Control {

    public static final String TAG = Control.class.getSimpleName();

    public static final boolean VERBOSE_ENABLED = Log.isLoggable(TAG, Log.VERBOSE);

    private final WorldModel mWorld;

    private NBodySurfaceView mView;

    private final Thread mThread;

    public Control(WorldModel world, NBodySurfaceView nBodySurfaceView) {
        mWorld = world;
        mView = nBodySurfaceView;
        mThread = new Thread(new ControllerRunnable(), "Controller");
        mThread.setDaemon(true);
    }

    public void start() {
        if (mThread.isAlive()) {
            return;
        }
        mThread.start();
    }

    public boolean onTouch(MotionEvent event) {
        Log.i("Control", "Touch");
        return true;
    }

    private class ControllerRunnable implements Runnable {

        public void run() {
            final float width = mWorld.getTop().getWidth();
            final float height = mWorld.getLeft().getHeight();

            for (int i = 0; i < mWorld.getNumberOfBalls(); i++) {
                final double x = width * (float) Math.random() + mWorld.getLeft().getPosition().get(0);
                final double y = height * (float) Math.random() + mWorld.getBottom().getPosition().get(1);
                final double z = 1;
                final double mass = 1;
                final Planet newPlanet = new Planet(mass, x, y, z);
                mWorld.addDynamicObject(newPlanet);
            }
            mView.requestRender();
            final List<Drawable> planets = mWorld.getDynamicObjects();

            final Vector[] force = new Vector[planets.size()];
            for (int i = 0; i < force.length; i++) {
                force[i] = new Vector(3);
            }

            while (!Thread.interrupted()) {

                for (int i = 0; i < planets.size(); i++) {
                    final Drawable planetI = planets.get(i);
                    final Body bodyI = planetI.getBody();
                    for (int j = 0; j < planets.size(); j++) {
                        if (i != j) {
                            final Body bodyJ = planets.get(j).getBody();
                            final Vector bodyJBodyIForce = bodyI.forceFrom(bodyJ);
                            force[i] = force[i].plus(bodyJBodyIForce);
                        }
                    }
                }
                for (int i = 0; i < planets.size(); i++) {
                    final Drawable planet = planets.get(i);
                    final Body body = planet.getBody();
                    
                    if (VERBOSE_ENABLED) {
                        Log.v(TAG, String.format("Moving body %s with force %s and DT - %d", body, force[i], Physics.DT));
                    }

                    body.move(force[i], Physics.DT);
                    final Vector position = body.getPosition();
                    if (body.leftOf(mWorld.getLeft())) {
                        position.set(0, mWorld.getLeft().getPosition().get(0));
                        final Vector velocity = body.getVelocity();
                        velocity.set(0, Math.abs(velocity.get(0)));
                    }
                    if (body.rightOf(mWorld.getRight())) {
                        position.set(0, mWorld.getRight().getPosition().get(0));
                        final Vector velocity = body.getVelocity();
                        velocity.set(0, -Math.abs(velocity.get(0)));
                    }
                    if (body.topOf(mWorld.getTop())) {
                        position.set(1, mWorld.getTop().getPosition().get(1));
                        final Vector velocity = body.getVelocity();
                        velocity.set(0, -Math.abs(velocity.get(0)));
                    }
                    if (body.bottomOf(mWorld.getBottom())) {
                        position.set(1, mWorld.getBottom().getPosition().get(1));
                        final Vector velocity = body.getVelocity();
                        velocity.set(0, Math.abs(velocity.get(0)));
                    }
                    force[i].clear();
                }

                mView.requestRender();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }
}
