package nf.co.ivelin.nbody.model;

import java.util.Arrays;

public class Vector {
	
	/** Vector coordinates. */
	private final double[] mCoords;
	
	public Vector(int length) {
		mCoords = new double[length];
	}
	
	public Vector(double... coords) {
		mCoords = new double[coords == null ? 0 : coords.length]; 
		System.arraycopy(coords, 0, mCoords, 0, mCoords.length);
	}
	
	public double multiply(Vector v) {
		assertSimilar(v);
		double result = 0;
		for (int i=0;i<getSpace();i++) {
			result += mCoords[i] * v.mCoords[i];
		}
		return result;
	}


	/**
	 * Minus.
	 * 
	 * @param v Vector instance
	 * @return this -v
	 */
	public Vector minus(Vector v) {
		assertSimilar(v);
		final Vector result = new Vector(getSpace());
		for (int i=0;i<getSpace();i++) {
			result.set(i, mCoords[i] - v.mCoords[i]);
		}
		return result;
	}
	
	/**
	 * Plus.
	 * 
	 * @param v Vector instance
	 * @return this -v
	 */
	public Vector plus(Vector v) {
		assertSimilar(v);
		final Vector result = new Vector(getSpace());
		for (int i=0;i<getSpace();i++) {
			result.set(i, mCoords[i] + v.mCoords[i]);
		}
		return result;
	}
	
	/**
	 * @param v
	 * @return the distance between two vectors 
	 */
	public double distance(Vector v) {
		assertSimilar(v);
		return v.minus(this).getLength();
	}
	
    public Vector times(double factor) {
        Vector result = new Vector(getSpace());
        for (int i = 0; i < getSpace(); i++) {
            result.set(i, (double) (factor * mCoords[i]));
        }
        return result;
    }
	
    public Vector direction() {
        if (this.getLength() == 0.0) throw new RuntimeException("Zero-vector has no direction");
        return this.times(1.0f / this.getLength());
    }
	
	public double getLength() {
		return (double) Math.sqrt(multiply(this));
	}
	
	public int getSpace() {
		return mCoords.length;
	}
	
	
	private void assertSimilar(Vector v) {
		if (v == null || mCoords.length != v.getSpace()) {
			throw new IllegalArgumentException("Passed vector is either null or in a different space.");
		}
	}

	public double get(int index) {
		return mCoords[index];
	}
	
	public void set(int index, double coord) {
		mCoords[index] = coord;
	}

	@Override
	public String toString() {
		return String.format("Vector [mCoords=%s]", Arrays.toString(mCoords));
	}

	public void clear() {
		for (int i=0;i<getSpace();i++) {
			mCoords[i] = 0;
		}
	}

}
