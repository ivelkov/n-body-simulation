package nf.co.ivelin.nbody.model;

import java.util.ArrayList;
import java.util.List;

import nf.co.ivelin.nbody.view.gl.Drawable;

public class WorldModel {

    private final List<Drawable> mDynamicObjects = new ArrayList<Drawable>();
    
    private final List<Drawable> mStaticObjects = new ArrayList<Drawable>();

    private Body mLeft;
    private Body mRight;
    private Body mBottom;
    private Body mTop;

    private int mNumberOfBalls = 4;

    private Drawable mBackground;

    public WorldModel() {
    }

    public void addDynamicObject(Drawable planet) {
        mDynamicObjects.add(planet);
    }
    
    public void addStaticObject(Drawable object) {
        mStaticObjects.add(object);
    }
    
    public List<Drawable> getStaticObjects() {
        return mStaticObjects;
    }

    public List<Drawable> getDynamicObjects() {
        return mDynamicObjects;
    }

    public Body getLeft() {
        return mLeft;
    }

    public Body getRight() {
        return mRight;
    }

    public Body getBottom() {
        return mBottom;
    }

    public Body getTop() {
        return mTop;
    }

    public void setLeft(Body left) {
        this.mLeft = left;
    }

    public void setRight(Body right) {
        this.mRight = right;
    }

    public void setBottom(Body bottom) {
        this.mBottom = bottom;
    }

    public void setTop(Body top) {
        this.mTop = top;
    }

    public void setBackground(Drawable background) {
        mBackground = background;
    }

    public Drawable getBackground() {
        return mBackground;
    }

    public int getNumberOfBalls() {
        return mNumberOfBalls;
    }

}
