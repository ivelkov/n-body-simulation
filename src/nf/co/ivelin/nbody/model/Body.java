package nf.co.ivelin.nbody.model;

public class Body {

    private final double mMass;

    private Vector mPosition;

    private Vector mVelocity;

    private final double mFriction = 0.6;

    private final Vector mRotateVector = new Vector(0, 0, 0);

    private float mRotateAngle = 0;

    private final float mWidth;

    private final float mHeight;

    public Body() {
        this(1);
    }

    public Body(float mass) {
        this(mass, 0, 0, 0, 1, 1);
    }

    public Body(double mass, double x, double y, double z, float width, float height) {
        mMass = mass;
        mPosition = new Vector(x, y, z);
        mVelocity = new Vector(3);
        mWidth = width;
        mHeight = height;
    }

    public void rotate(float rotateAngle, float x, float y, float z) {
        mRotateAngle = rotateAngle;
        mRotateVector.set(0, x);
        mRotateVector.set(1, y);
        mRotateVector.set(2, z);
    }

    public void move(Vector force, double dt) {
        final Vector a = force.times(1 / mMass);
        mVelocity = mVelocity.plus(a.times(dt));
        mVelocity = mVelocity.times(mFriction);
        mPosition = mPosition.plus(mVelocity.times(dt));
    }

    public Vector forceFrom(Body body) {
        final Vector delta = body.getPosition().minus(this.getPosition());
        double length = delta.getLength();
        if (length == 0) {
            return new Vector(0, 0, 0);
        }
        if (Math.abs(length) < 0.5) {
            length = 0.5;
        }
        final double F = (Physics.G * getMass() * body.getMass()) / (length * length);
        return delta.direction().times(F);
    }

    public boolean hitTest(Body body) {
        if ((getPosition().get(0) + getWidth() >= body.getPosition().get(0)) && (getPosition().get(0) <= body.getPosition().get(0) + body.getWidth())
                && (getPosition().get(1) + getHeight() >= body.getPosition().get(1) && (getPosition().get(1) <= body.getPosition().get(1) + body.getHeight()))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean leftOf(Body body) {
        return getPosition().get(0) < body.getPosition().get(0);
    }

    public boolean rightOf(Body body) {
        return getPosition().get(0) > body.getPosition().get(0);
    }

    public boolean topOf(Body body) {
        return getPosition().get(1) > body.getPosition().get(1);
    }

    public boolean bottomOf(Body body) {
        return getPosition().get(1) < body.getPosition().get(1);
    }

    /**
     * @return the x
     */
    public Vector getPosition() {
        return mPosition;
    }

    /**
     * @return the mass
     */
    public double getMass() {
        return mMass;
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }

    public Vector getRotateVector() {
        return mRotateVector;
    }

    public float getRotateAngle() {
        return mRotateAngle;
    }

    public Vector getVelocity() {
        return mVelocity;
    }

    @Override
    public String toString() {
        return String.format("Body [mPosition=%s, mVelocity=%s, mMass=%s]", mPosition, mVelocity, mMass);
    }

}
